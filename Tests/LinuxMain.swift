import XCTest
@testable import PulseTests

XCTMain([
     testCase(PulseTests.allTests),
])
