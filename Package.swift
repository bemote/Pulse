import PackageDescription

let package = Package(
    name: "Pulse",
    dependencies:[
        .Package(url: "git@gitlab.com:boterock/CPulse.git",
                 majorVersion: 1),
    ]
)
