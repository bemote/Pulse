import CPulse

typealias CVolume = pa_cvolume


extension CVolume {
    mutating func set(volume: Double)
    {
        self.set(volume: volume, channels: self.channels)
    }
    mutating func set(volume: Double, channels:UInt8)
    {
        let vol = UInt32(volume * Double(PA_VOLUME_NORM))
        // print(vol)
        pa_cvolume_set(&self, UInt32(channels), vol)
    }

    public var max: Double { 
        mutating get {
            return Double(pa_cvolume_max(&self))/Double(PA_VOLUME_NORM)
        }
    }
    
}